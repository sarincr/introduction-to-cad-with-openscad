# Introduction to CAD-with-OpenSCAD

OpenSCAD is a free software application for creating solid 3D CAD objects. It is a script-only based modeller that uses its own description language; parts can be previewed, but cannot be interactively selected or modified by mouse in the 3D view.